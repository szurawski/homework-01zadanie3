package pl.codementors.zoo;

public class Iguana extends Lizard implements Herbivorous{

    public Iguana(String name, int age, String scaleColor) {
        super(name, age, scaleColor);
    }


    public void hiss(){
        System.out.println(getName()+" Hiss");
    }

    @Override
    public void eat() {
        System.out.println(getName()+" Eat");
    }

    public void eatPlant() {
        System.out.println("morePlants");
    }
}
