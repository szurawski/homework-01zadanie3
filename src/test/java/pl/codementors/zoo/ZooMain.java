package pl.codementors.zoo;

import java.security.Principal;
import java.util.Arrays;
import java.io.*;
import java.util.Scanner;

public class ZooMain {


    public static void main(String[] args) {
        //zad4
        System.out.println("zad04");
        System.out.println("Hello to the ZOO\n");

        //zad8
        System.out.println("zad08");
        Scanner inputScanner = new Scanner(System.in);
        System.out.println("Ile zwierzątek chcesz wprowadzić??");
        int size = inputScanner.nextInt();
        Animal[] tablicaZwierzat = new Animal[size];

        int i;

        for (i = 0; i < tablicaZwierzat.length; i++) {
            System.out.println("Jakie zwierze chcesz dodac? ");
            System.out.print("1. Wolf.\t2. Iguana.\t3. Parrot.");
            int wyborZwierzecia = inputScanner.nextInt();
            System.out.print("Podaj imie: ");
            String name = inputScanner.next();
            //Wieku nie chciałeś
            int age = 1;
            //System.out.print("Podaj wiek: ");
            //int age = inputScanner.nextInt();
            System.out.print("Podaj kolor: ");


            if (wyborZwierzecia == 1) {

                String furColor = inputScanner.next();
                Animal animal;
                animal = new Wolf(name, age, furColor);
                tablicaZwierzat[i] = animal;
            } else if (wyborZwierzecia == 2) {

                String scaleColor = inputScanner.next();
                Animal animal;
                animal = new Iguana(name, age, scaleColor);
                tablicaZwierzat[i] = animal;
            } else if (wyborZwierzecia == 3) {

                String featherColor = inputScanner.next();
                Animal animal;
                animal = new Parrot(name, age, featherColor);
                tablicaZwierzat[i] = animal;
            }


        }
        //Zad09
        System.out.println("\nzad09");
        System.out.println("\nW tablicy posiadasz:\n");
        ZooMain.print(tablicaZwierzat);
        //Zad10
        System.out.println("\nzad10");
        ZooMain.feed(tablicaZwierzat);
        //Zad11
        System.out.println("\nzad11");
        String nameFile = "tablicaTekstowa";
        saveToFile(tablicaZwierzat, nameFile);
        //zad14
        System.out.println("\nzad14");
        ZooMain.howl(tablicaZwierzat);
        //zad15
        System.out.println("\nzad15");
        ZooMain.hiss(tablicaZwierzat);
        //zad16
        System.out.println("\nzad16");
        ZooMain.screech(tablicaZwierzat);
        //zad17
        System.out.println("\nzad17");
        ZooMain.feedWithMeat(tablicaZwierzat);
        //zad18
        System.out.println("\nzad18");
        ZooMain.feedWithPlant(tablicaZwierzat);
        //zad19
        System.out.println("\nzad19");
        Animal[] tablicaZwierzatZTxt = readFromFile("tablicaTekstowa");
        if (tablicaZwierzatZTxt != null) {
            System.out.println("Tablica z pliku tekstowego:");
            print(tablicaZwierzatZTxt);

        }
        //zad20
        System.out.println("\nzad20");
        printColors(tablicaZwierzatZTxt);

        //zad21
        System.out.println("\nzad21");
        String nazwaPlikuBinarnego = "tablicaBinarnie";
        saveToBinaryFile(tablicaZwierzat, nazwaPlikuBinarnego);

        //zad22
        System.out.println("\nzad22");


    }


    private static void print(Animal[] tablicaZwierzat) {
        for (int i = 0; i < tablicaZwierzat.length; i++) {
            System.out.println("Zwierze na pozycji " + i);
            opis(tablicaZwierzat[i]);
        }
    }

    private static void opis(Animal animal) {
        String gatunek = null;
        if (animal instanceof Wolf) {
            gatunek = "Wolf";
        } else if (animal instanceof Iguana) {
            gatunek = "Iguana";
        } else if (animal instanceof Parrot) {
            gatunek = "Parrot";
        }
        System.out.print("Gatunek: " + gatunek);
        System.out.println("\tImię: " + animal.getName());

    }

    private static void feed(Animal[] tablicaZwierzat) {
        for (int i = 0; i < tablicaZwierzat.length; i++) {
            if (tablicaZwierzat[i] != null) {
                tablicaZwierzat[i].eat();
            }
            //for (Animal a : tablicaZwierzat) {
            //   a.eat();
        }
    }

    static void saveToFile(Animal[] tablicaZwierzat, String sciezka) {

        File plik = new File("/tmp/" + sciezka + ".txt");

        if (!plik.exists()) {
            System.out.println("Udany zapisu do pliku");
            FileWriter fw = null;
            try {
                fw = new FileWriter(plik);

                fw.write(tablicaZwierzat.length + "\n");
                for (int i = 0; i < tablicaZwierzat.length; i++) {
                    if (tablicaZwierzat[i] != null) {
                        if (tablicaZwierzat[i] instanceof Wolf) {


                            fw.write("Wolf " + tablicaZwierzat[i].getName() + "\n");

                        } else if (tablicaZwierzat[i] instanceof Iguana) {

                            fw.write("Iguana " + tablicaZwierzat[i].getName() + "\n");

                        } else if (tablicaZwierzat[i] instanceof Parrot) {

                            fw.write("Parrot " + tablicaZwierzat[i].getName() + "\n");

                        }
                    }

                }

            } catch (IOException ex) {

                System.out.println(ex);
            } finally {

                if (fw != null) {
                    try {
                        fw.close();
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                }
            }

        } else {
            System.out.println("ERROR: plik o nazwie " + sciezka + ".txt już istnieje.");
        }
    }

    static void howl(Animal[] tablicaZwierzat) {
        for (int i = 0; i < tablicaZwierzat.length; i++) {
            if (tablicaZwierzat[i] != null) {
                if (tablicaZwierzat[i] instanceof Wolf) {
                    ((Wolf) tablicaZwierzat[i]).howl();
                }
            }
        }

    }

    static void hiss(Animal[] tablicaZwierzat) {
        for (int i = 0; i < tablicaZwierzat.length; i++) {
            if (tablicaZwierzat[i] != null) {
                if (tablicaZwierzat[i] instanceof Iguana) {
                    ((Iguana) tablicaZwierzat[i]).hiss();
                }
            }
        }
    }

    static void screech(Animal[] tablicaZwierzat) {
        for (int i = 0; i < tablicaZwierzat.length; i++) {
            if (tablicaZwierzat[i] != null) {
                if (tablicaZwierzat[i] instanceof Parrot) {
                    ((Parrot) tablicaZwierzat[i]).screech();
                }
            }
        }
    }

    static void feedWithMeat(Animal[] tablicaZwierzat) {
        for (int i = 0; i < tablicaZwierzat.length; i++) {
            if (tablicaZwierzat[i] != null) {
                if (tablicaZwierzat[i] instanceof Carnivorous) {
                    ((Carnivorous) tablicaZwierzat[i]).eatMeat();
                }
            }
        }
    }

    static void feedWithPlant(Animal[] tablicaZwierzat) {
        for (int i = 0; i < tablicaZwierzat.length; i++) {
            if (tablicaZwierzat[i] != null) {
                if (tablicaZwierzat[i] instanceof Herbivorous) {
                    ((Herbivorous) tablicaZwierzat[i]).eatPlant();
                }
            }
        }
    }

    static Animal[] readFromFile(String nazwaPliku) {

        Animal[] tablicaWczytanychZwierzat = null;

        try (BufferedReader br = new BufferedReader(new FileReader(new File("/tmp/" + nazwaPliku + ".txt")))) {
            while (br.ready()) {//True gdy jest cos do wczytania
                //wczytanie pierwszej linii i ustalenie wielkosci tablicy
                String indexText = br.readLine();
                int iloscZwierzatWPliku = Integer.parseInt(indexText);
                tablicaWczytanychZwierzat = new Animal[iloscZwierzatWPliku];
                for (int i = 0; i < iloscZwierzatWPliku; i++) {
                    //wczytanie lini i podzial stringa
                    String zwierzeRodzajImie = br.readLine();
                    String[] zwierzePodzial = zwierzeRodzajImie.split("\\s+");
                    //sprawdzenie zawartosci tablicy srtingow z podzialu
                    if (zwierzePodzial[0].contains("Wolf")) {
                        //dodanie wilka do nowej tablicy, brak koloru i wieku w zapisanym pliku txt
                        tablicaWczytanychZwierzat[i] = new Wolf(zwierzePodzial[1], 1, "BrakDanych");
                    } else if (zwierzePodzial[0].contains("Iguana")) {
                        //dodanie legwana do nowej tablicy, brak koloru i wieku w zapisanym pliku txt
                        tablicaWczytanychZwierzat[i] = new Iguana(zwierzePodzial[1], 1, "BrakDanych");
                    } else if (zwierzePodzial[0].contains("Parrot")) {
                        //dodanie papugi do nowej tablicy, brak koloru i wieku w zapisanym pliku txt
                        tablicaWczytanychZwierzat[i] = new Parrot(zwierzePodzial[1], 1, "BrakDanych");
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }


        return tablicaWczytanychZwierzat;
    }

    static void printColors(Animal[] tablicaZwierzat) {

        for (int i = 0; i < tablicaZwierzat.length; i++) {
            if (tablicaZwierzat[i] != null) {
                if (tablicaZwierzat[i] instanceof Mammal) {

                    System.out.println((i + 1) + ". Wolf  " + tablicaZwierzat[i].getName() + " i kolorze futra " + ((Mammal) tablicaZwierzat[i]).getFurColor() + ".");

                } else if (tablicaZwierzat[i] instanceof Lizard) {
                    //wypisanie gdy jaszczurka
                    System.out.println((i + 1) + ". Iguana  " + tablicaZwierzat[i].getName() + " i kolorze łusek " + ((Lizard) tablicaZwierzat[i]).getScaleColor() + ".");

                } else if (tablicaZwierzat[i] instanceof Bird) {
                    //wypisanie gdy ptak
                    System.out.println((i + 1) + ". Parrot  " + tablicaZwierzat[i].getName() + " i kolorze piór " + ((Bird) tablicaZwierzat[i]).getFeatherColor() + ".");

                }
            }

        }

    }

    static void saveToBinaryFile(Animal[] tablicaZwierzat, String sciezka) {

        File plik = new File("/tmp/" + sciezka);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(plik))) {
            oos.writeObject(tablicaZwierzat);
        } catch (IOException ex) {
            System.err.println(ex);
        }


    }
}



