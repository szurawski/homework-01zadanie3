package pl.codementors.zoo;

public interface Herbivorous {

    void eatPlant();

}
