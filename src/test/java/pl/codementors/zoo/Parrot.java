package pl.codementors.zoo;

public class Parrot extends Bird implements Herbivorous{

    public Parrot(String name, int age, String featherColor) {
        super(name, age, featherColor);
    }

    public void screech(){
        System.out.println(getName()+" Screech");
    }

    @Override
    public void eat() {
        System.out.println(getName()+" Eat");
    }

    public void eatPlant() {

        System.out.println("morePlants");

    }
}
