package pl.codementors.zoo;

public abstract class Bird extends Animal{

    private String featherColor;

    public Bird(String name, int age, String featherColor) {
        super(name, age);
        this.featherColor = featherColor;
    }

    public Bird(String name, int age) {
        super(name, age);
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }
}
