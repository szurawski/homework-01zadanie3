package pl.codementors.zoo;

public class Wolf extends Mammal implements Carnivorous {


    public Wolf(String name, int age, String furColor) {
        super(name, age, furColor);
    }

    public void howl(){
        System.out.println(getName()+" Howl");
    }

    @Override
    public void eat() {
        System.out.println(getName()+" Eat");
    }


    public void eatMeat() {
        System.out.println("moreMeat");
    }
}
